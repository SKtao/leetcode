class LinkedNode{
    int key;
    int val;
    LinkedNode pre;
    LinkedNode next;
    
    public LinkedNode(int key,int val){
        this.key = key;
        this.val= val;
    }
}
class LRUCache {
    
    private int capacity;
    private int size;
    private Map<Integer,LinkedNode>cache;
    private LinkedNode head;
    private LinkedNode tail;
    
    
    public LRUCache(int capacity) {
        this.capacity = capacity;
        cache = new HashMap<Integer,LinkedNode>();
        head = new LinkedNode(-1,-1);
        tail = new LinkedNode(-1,-1);
        head.next = tail;
        tail.pre = head;
        head.pre = null;
        tail.next = null;
        size = 0;
    }
    
    public int get(int key) {
        if(cache.containsKey(key)){
            LinkedNode node = cache.get(key);
            deleteNode(node);
            addToHead(node);
            return node.val;
        }
        return -1;
    }
    
    public void put(int key, int value) {
        if(cache.containsKey(key)){
            LinkedNode node = cache.get(key);
            node.val = value;
            deleteNode(node);
            addToHead(node);
        }
        else{
            LinkedNode node = new LinkedNode(key,value);
            cache.put(key,node);
            if(size < capacity){
                size++;
                addToHead(node);
            }else{
                cache.remove(tail.pre.key);
                deleteNode(tail.pre);
                addToHead(node);
            }
        }
    }
    //delete Node
    private void deleteNode(LinkedNode node){
        node.pre.next = node.next;
        node.next.pre = node.pre;
    }
    // add Node to Head
    private void addToHead(LinkedNode node){
        node.next = head.next;
        node.next.pre = node;
        node.pre = head;
        head.next = node;
    }
}

