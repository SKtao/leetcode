/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    private int maxSum = Integer.MIN_VALUE;
    
    public int maxPathSum(TreeNode root) {
        Find(root);
        return maxSum;
    }
    
    private int Find(TreeNode root){
        if(root == null) return 0;
        int left = Math.max(0,Find(root.left));
        int right = Math.max(0,Find(root.right));
        maxSum = Math.max(maxSum,root.val+left+right);
        return root.val+Math.max(left,right);
    }
}