/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
//注：尽量简化代码，灵活使用三元表达式
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode dummy = new ListNode(0),pNode = dummy;
        int carry = 0;
        while(l1 != null || l2 != null || carry == 1){
            int sum = (l1 == null ? 0 : l1.val) + (l2 == null ? 0 : l2.val) + carry;
            carry = sum/10;
            pNode.next = new ListNode(sum % 10);
            l1 = (l1 != null) ? l1.next : l1;
            l2 = (l2 != null) ? l2.next : l2;
            pNode = pNode.next;
        }
        pNode.next = null;
        return dummy.next;
    }
}