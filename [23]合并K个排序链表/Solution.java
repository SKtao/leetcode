/* 
class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
} 
*/

class Solution {
    public ListNode mergeKLists(ListNode[] lists) {
        int length = lists.length;
        switch(length){
            case 0:return null;
            case 1:return lists[0];
            case 2:return mergeTwoList(lists[0],lists[1]);
            default:;
        }
        int mid = length/2;
        ListNode[] leftLists = subArray(lists,0,mid);
        ListNode[] rightLists = subArray(lists,mid,length);
        //递归合并
        return mergeTwoList(mergeKLists(leftLists),mergeKLists(rightLists));
    }
    
    private ListNode[] subArray(ListNode[] lists, int left, int right) {
        ListNode[] result = new ListNode[right-left];
        for(int i = left ; i<right ; i++){
            result[i-left] = lists[i];
        }
        return result;

    }

    //合并两个链表
    private ListNode mergeTwoList(ListNode l1, ListNode l2){
        if (l1 == null){
            return l2;
        }
        if (l2 == null){
            return l1;
        }
        ListNode head = null;
        if (l1.val <= l2.val){
            head = l1;
            head.next = mergeTwoList(l1.next,l2);
        }
        else {
            head = l2;
            head.next = mergeTwoList(l1,l2.next);
        }
        return head;
    }
}